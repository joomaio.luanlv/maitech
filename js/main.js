$(document).ready(function(){
    $('.bar-toggle').on('click' , function() {
        $('.toggle-menu').animate({left: '10px'});
    });
    $('.close').on('click' , function() {
        $('.toggle-menu').animate({left: '-300px'});
    });
    $('.btn-follow').on('click' , function() {
        $('.popup').removeClass('d-none');
    });
    $('.overlay').on('click' , function(e) {
        $('.popup').addClass('d-none');
    });
});